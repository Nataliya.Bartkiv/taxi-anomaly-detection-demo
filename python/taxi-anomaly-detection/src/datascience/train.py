# Imports
import numpy as np
import shutil, os
import pickle
from argparse import ArgumentParser
import pydoop.hdfs as hdfs
from pyspark.sql import SparkSession
from pyspark.ml.clustering import GaussianMixture, GaussianMixtureModel

from preparation.data_preparation import DataPreparator


def minMaxSeqOp(agg, data):
    mins = agg[0]
    maxes = agg[1]
    length = len(data[1])

    new_mins = [None] * length
    new_maxes = [None] * length

    for index, value in enumerate(data[1]):
        new_mins[index] = min(mins[index], value)
        new_maxes[index] = max(maxes[index], value)
    return (new_mins, new_maxes)


def minMaxCombOp(agg1, agg2):
    mins1 = agg1[0]
    maxes1 = agg1[1]

    mins2 = agg2[0]
    maxes2 = agg2[1]

    length = len(mins1)
    mins = [None] * length
    maxes = [None] * length
    for index, value in enumerate(mins2):
        mins[index] = min(mins1[index], mins2[index])
        maxes[index] = max(maxes1[index], maxes2[index])
    return (mins, maxes)


def get_mins_maxes(rdd):
    mins = rdd.first()[1]
    maxes = mins
    return rdd.aggregate((mins, maxes), minMaxSeqOp, minMaxCombOp)


def train(trainDataset, kernels, seed):
    gm = GaussianMixture().setK(kernels).setSeed(seed)
    model = gm.fit(trainDataset)
    transformed = model.transform(trainDataset)
    transformed.show()

    return model, gm


def save_model(path, model, gm, mins, maxes):
    model_folder = path + "/model/"
    gm_folder = path + "/gm/"
    mins_file = path + "/mins.p"
    maxes_file = path + "/maxes.p"

    # shutil.rmtree(path, ignore_errors=True)
    # os.mkdir(path)

    model.write().overwrite().save(model_folder)
    gm.write().overwrite().save(gm_folder)

    pickle.dump(mins, open(mins_file, "wb"))
    pickle.dump(maxes, open(maxes_file, "wb"))

    mins_command = 'echo "%s" | hadoop fs -put -f - ' + mins_file
    os.system(mins_command % (pickle.dumps(mins)))

    maxes_command = 'echo "%s" | hadoop fs -put -f - ' + maxes_file
    os.system(maxes_command % (pickle.dumps(maxes)))


# Main
if __name__ == "__main__":
    parser = ArgumentParser(description='Training anomaly detection model fot anomaly detection.')
    parser.add_argument('--input_path', type=str, help='Path to raw input dataset')
    parser.add_argument('--labels_count', type=int,
                        help='Amount of "dimensions" - columns that will not infuence the training process')
    parser.add_argument('--kernels', type=int, help='Supposed amount of groups in data', default=3)
    parser.add_argument('--seed', type=int, help='Seed for model training', default=500000)
    parser.add_argument('--model_path', type=str,
                        help='Path to directoru where trained model will be stored. Note: old model will be OVERWRITTEN.')
    args = parser.parse_args()

    app_name = 'Taxi Anomaly Detection Model Training'
    spark = SparkSession \
        .builder \
        .appName(app_name) \
        .getOrCreate()

    preparator = DataPreparator(spark)

    initRDD, header = preparator.read_csv_and_map(args.input_path, args.labels_count)
    mins, maxes = get_mins_maxes(initRDD)
    normalizedRDD = preparator.normalize(initRDD, mins, maxes)
    preparedDF = preparator.to_train_df(normalizedRDD)

    model, gm = train(preparedDF, args.kernels, args.seed)
    save_model(args.model_path, model, gm, mins, maxes)
