from __future__ import print_function

from argparse import ArgumentParser

from pyspark.sql import SparkSession
from pyspark.ml.clustering import KMeans
from pyspark.ml.evaluation import ClusteringEvaluator

from preparation.data_preparation import DataPreparator


def minMaxSeqOp(agg, data):
    mins = agg[0]
    maxes = agg[1]
    length = len(data[1])

    new_mins = [None] * length
    new_maxes = [None] * length

    for index, value in enumerate(data[1]):
        new_mins[index] = min(mins[index], value)
        new_maxes[index] = max(maxes[index], value)
    return (new_mins, new_maxes)


def minMaxCombOp(agg1, agg2):
    mins1 = agg1[0]
    maxes1 = agg1[1]

    mins2 = agg2[0]
    maxes2 = agg2[1]

    length = len(mins1)
    mins = [None] * length
    maxes = [None] * length
    for index, value in enumerate(mins2):
        mins[index] = min(mins1[index], mins2[index])
        maxes[index] = max(maxes1[index], maxes2[index])
    return (mins, maxes)


def get_mins_maxes(rdd):
    mins = rdd.first()[1]
    maxes = mins
    return rdd.aggregate((mins, maxes), minMaxSeqOp, minMaxCombOp)


def get_kernel_count(trainDataset):
    MIN_KERNELS = 2
    MAX_KERNELS = 7

    metrics = dict()
    for kernel_count in range(MIN_KERNELS, MAX_KERNELS):
        kmeans = KMeans().setK(kernel_count)
        model = kmeans.fit(trainDataset)
        predictions = model.transform(trainDataset)
        evaluator = ClusteringEvaluator()
        silhouette = evaluator.evaluate(predictions)
        metrics[kernel_count] = silhouette
        print(
            "Kernel count = " + str(kernel_count) + "; Silhouette with squared euclidean distance = " + str(silhouette))

    optimal_kernel_count = int(max(metrics, key=metrics.get))
    return optimal_kernel_count


if __name__ == "__main__":
    parser = ArgumentParser(description='Finding out perfect kernel count for dataset with K-Means Silhouette Metrics')
    parser.add_argument('--input_path', type=str, help='Path to input dataset')
    parser.add_argument('--labels_count', type=int,
                        help='Amount of "dimensions" - columns that will not infuence the prediction process',
                        default=6)
    args = parser.parse_args()

    app_name = 'K Means Silhouette Metrics'
    spark = SparkSession.builder.appName(app_name).getOrCreate()

    preparator = DataPreparator(spark)

    initRDD, header = preparator.read_csv_and_map(args.input_path, args.labels_count)
    mins, maxes = get_mins_maxes(initRDD)
    normalizedRDD = preparator.normalize(initRDD, mins, maxes)
    trainDataset = preparator.to_train_df(normalizedRDD)

    optimal_kernel_count = get_kernel_count(trainDataset)

    print("Best kernel count = " + str(optimal_kernel_count))