import numpy as np
import pyspark.sql.functions as F
from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark.ml.linalg import SparseVector
from pyspark.sql.types import FloatType

from preparation.data_aggregation import aggregate

class DataPreparator:
    def __init__(self, spark):
        self.spark = spark

    def aggregate_parquet_and_map(self, input_path, labels_count):
        raw_data = self.spark.read.parquet(input_path)
        agg_data = aggregate(raw_data)
        aggRDD = agg_data.rdd

        header = agg_data.columns
        column_count = len(header)

        def df_to_rdd(row):
            labels = tuple()
            features = tuple()

            for index in range(column_count):
                token = row[index]
                if index < labels_count:
                    labels = labels + (token,)
                else:
                    features = features + ((0.0 if token == None else float(token)),)
            return labels, features

        mappedRDD = aggRDD.map(df_to_rdd)
        return mappedRDD, header

    def map_csv(self, inputRDD, labels_count):
        def initial_map(line):
            tokens = line.split(',')
            labels = tuple()
            features = tuple()

            for index, token in enumerate(tokens):
                if (index < labels_count):
                    labels = labels + (token,)
                else:
                    features = features + ((0.0 if token == '' else float(token)),)
            return (labels, features)

        mappedRDD = inputRDD.map(initial_map)
        return mappedRDD

    def read_csv_and_map(self, input_path, labels_count):
        inputRDD = self.spark.sparkContext.textFile(input_path)

        header = inputRDD.first()
        inputRDD = inputRDD.filter(lambda line: line != header)

        mappedRDD = self.map_csv(inputRDD, labels_count)
        return mappedRDD, header

    def normalize(self, inputRDD, mins, maxes):
        def normalize_row(data):
            labels = data[0]
            features = data[1]

            normalized_features = [None] * len(features)
            for index, value in enumerate(features):
                denominator = (maxes[index] - mins[index])
                normalized = (value - mins[index]) / denominator if denominator > 0 else 0.0
                normalized_features[index] = normalized
            return labels, normalized_features

        normalizedRDD = inputRDD.map(normalize_row)
        return normalizedRDD

    def to_train_df(self, rdd):
        def to_sparse_vector(values):
            labels = values[0]
            features = values[1]

            size = len(features)
            indices = range(0, size)
            return (labels, SparseVector(size, indices, features))

        sparseVectorsRDD = rdd.map(to_sparse_vector)
        trainDataset = sparseVectorsRDD.toDF(["labels", "features"])
        return trainDataset

    def add_anomaly_probability(self, df, kernels):
        # Define custom udf functions
        def maxList(list, index):
            return list.item(index)

        def getAnomalyProbability(probability, kernel_count):
            min_border = 1.0 / kernel_count
            max_border = 1.0
            true_probability = (probability - min_border) / (max_border - min_border)
            return 1 - true_probability

        # Register defined udfs
        maxUdf = F.udf(maxList, FloatType())
        getAnomalyProbability = F.udf(getAnomalyProbability, FloatType())

        # Call udfs to get max probability and after that - anomaly probability
        df = df.withColumn('max_probability', maxUdf('probability', 'prediction'))
        df = df.withColumn('anomaly_probability', getAnomalyProbability('max_probability', F.lit(kernels)))

        return df

    def prepare_data(self, input_path, labels_count, mins, maxes):
        initRDD, header = read_csv_and_map(input_path, labels_count)
        normalizedRDD = normalize(inputRDD, mins, maxes)
        preparedDF = to_train_df(normalizedRDD)

        return preparedDF, header

    def prepare_streaming_data(self, rdd, labels_count, mins, maxes):
        mappedRDD = self.map_csv(rdd, labels_count)
        normalizedRDD = self.normalize(mappedRDD, mins, maxes)
        preparedDF = self.to_train_df(normalizedRDD)

        return preparedDF