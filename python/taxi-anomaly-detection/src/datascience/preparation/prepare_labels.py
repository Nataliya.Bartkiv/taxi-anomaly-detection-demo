from argparse import ArgumentParser

from pyspark.sql import SparkSession
import pyspark.sql.functions as F
import os


def extract_values_with_frequencies(df, column_name):
    total_count = df.where(df[column_name].isNotNull()).count()
    # print(column_name + "'s total count = " + str(total_count))

    info_df = df.where(df[column_name].isNotNull()) \
        .groupBy(column_name) \
        .count() \
        .orderBy(column_name)
    info_df = info_df.withColumn('frequency', F.col('count') / F.lit(total_count)) \
        .withColumnRenamed(column_name, 'name') \
        .drop('count')
    # info_df.show()
    return info_df


def prepare_labels(spark, input_path, export_path, columns):
    df = spark.read.parquet(input_path)
    for column_name in columns:
        column_export_path = os.path.join(export_path, column_name)
        values_frequencies = extract_values_with_frequencies(df, column_name)
        values_frequencies.repartition(1).write.mode("overwrite").csv(column_export_path)


if __name__ == "__main__":
    parser = ArgumentParser(description='Preparing raw taxi dataset for anomaly detection.')
    parser.add_argument('--input_path', type=str, help='Path to raw input dataset')
    parser.add_argument('--output_path', type=str,
                        help='Path to directory where label values and frequencies will be stored.  Note: old data will be OVERWRITTEN.')
    parser.add_argument('--columns', type=str,
                        help='Comma-delimited list of columns-labels to extract values and frequencies of.')
    args = parser.parse_args()

    app_name = 'Extract values and frequencies.'
    spark = SparkSession \
        .builder \
        .appName(app_name) \
        .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    columns = args.columns.split(',')

    prepare_labels(spark, args.input_path, args.output_path, columns)

# spark-submit prepare_labels.py --input_path ../data/raw-parquet-small --output_path ../data/labels --columns dropoff_taxizone_id,payment_type,pickup_taxizone_id,rate_code_id,store_and_fwd_flag,trip_type,vendor_id


