from argparse import ArgumentParser

from pyspark.sql import SparkSession
import pyspark.sql.functions as F


def aggregate(raw_data):
    pickup_datetime = F.unix_timestamp('pickup_datetime')
    dropoff_datetime = F.unix_timestamp('dropoff_datetime')

    pickup_date = F.from_unixtime(pickup_datetime, format='yyyy-MM-dd').alias('pickup_date')
    pickup_hour = F.from_unixtime(pickup_datetime, format='HH').alias('pickup_hour')

    dropoff_date = F.from_unixtime(dropoff_datetime, format='yyyy-MM-dd').alias('dropoff_date')
    dropoff_hour = F.from_unixtime(dropoff_datetime, format='HH').alias('dropoff_hour')

    total_amount = F.sum('total_amount').alias('total_amount')
    trip_distance = F.sum('trip_distance').alias('trip_distance')
    passenger_count = F.sum('passenger_count').alias('passenger_count')
    tip_amount = F.sum('tip_amount').alias('tip_amount')
    trip_count = F.count('trip_id').alias('trip_count')

    agg_data = raw_data.groupBy(pickup_date, pickup_hour, raw_data.pickup_taxizone_id,
                                dropoff_date, dropoff_hour, raw_data.dropoff_taxizone_id) \
        .agg(total_amount, trip_distance, passenger_count,
             tip_amount, trip_count)
    return agg_data


def aggregate_and_save(spark, input_path, output_path, show=False):
    raw_data = spark.read.parquet(input_path)
    agg_data = aggregate(raw_data)

    if show:
        print("Aggregated data: ")
        agg_data.show()

    agg_data.write.mode("overwrite").csv(output_path, header=True)
    

if __name__ == "__main__":
    parser = ArgumentParser(description='Preparing raw taxi dataset for anomaly detection.')
    parser.add_argument('--input_path', type=str, help='Path to raw input dataset')
    parser.add_argument('--output_path', type=str,
                        help='Path to directory where aggregated data will be stored. Note: old data will be OVERWRITTEN.')
    args = parser.parse_args()

    app_name = 'Taxi Data Aggregation'
    spark = SparkSession \
        .builder \
        .appName(app_name) \
        .getOrCreate()

    aggregate_and_save(spark, args.input_path, args.output_path)