from argparse import ArgumentParser

from pyspark.sql import SparkSession
import pyspark.sql.functions as F


def get_avg_and_variance(df, column_name):
	avg_val = df.agg(F.avg(df[column_name])).first()[0]
	total_count = df.count()

	diff_col = F.pow((df[column_name] - avg_val), 2)

	variance_col = df.agg(F.sum(diff_col) / F.lit(total_count))
	variance_val = variance_col.first()[0]

	return avg_val, variance_val


def prepare_features(spark, input_path, export_path, columns, with_trip_duration = False):
	df = spark.read.parquet(input_path)

	if with_trip_duration:
		df = with_trip_duration_column(spark, df)
		columns.append("trip_duration")

	features_metadata = list()

	for column_name in columns:
		average, variance = get_avg_and_variance(df, column_name)
		features_metadata.append((column_name, average, variance))

	output_df = spark.createDataFrame(features_metadata, ["name", "average", "variance"])
	output_df.repartition(1).write.mode("overwrite").csv(export_path)


def with_trip_duration_column(spark, df):
	dropoff_name = "dropoff_datetime"
	pickup_name = "pickup_datetime"
	trip_duration_name = "trip_duration"

	dropoff_col = df[dropoff_name].cast("timestamp").cast("bigint")
	pickup_col = df[pickup_name].cast("timestamp").cast("bigint")

	trip_duration_col = dropoff_col - pickup_col
	time_df = df.withColumn(trip_duration_name, trip_duration_col)
	return time_df


def get_trip_duration_values(spark, df):
	time_df = with_trip_duration_column(spark, df)
	average, variance = get_avg_and_variance(time_df, trip_duration_name)
	return average, variance


if __name__ == "__main__":
	parser = ArgumentParser(description = 'Preparing raw taxi dataset for anomaly detection.')
	parser.add_argument('--input_path', type = str, help = 'Path to raw input dataset')
	parser.add_argument('--output_path', type = str, help = 'Path to directory/file where feature averages and variances will be stored.  Note: old data will be OVERWRITTEN.')
	parser.add_argument('--columns', type = str, help = 'Comma-delimited list of columns-features to extract averages and variances of.')
	parser.add_argument('--with_trip_duration', help = 'Flag that defines wheather to count avg and variance for trip duration.', action="store_true")
	args = parser.parse_args()

	app_name = 'Extract averages and variances.'
	spark = SparkSession \
		.builder \
		.appName(app_name) \
		.getOrCreate()
	spark.sparkContext.setLogLevel("ERROR")

	columns = args.columns.split(',')

	prepare_features(spark, args.input_path, args.output_path, columns, args.with_trip_duration)

	# spark-submit prepare_features.py --input_path ../data/raw-parquet-small --output_path ../data/features --columns total_amount,tip_amount,trip_distance,passenger_count --with_trip_duration


