from __future__ import print_function

from argparse import ArgumentParser
import pickle
import pandas as pd
from collections import namedtuple
from pyspark.sql import SparkSession
from pyspark.ml.clustering import GaussianMixtureModel, GaussianMixture
import pyspark.sql.functions as F

from preparation.data_preparation import DataPreparator


def load(path):
    spark_loader_path = 'file://' + path if path[0] == '/' else path

    model_folder = spark_loader_path + "/model/"
    gm_folder = spark_loader_path + "/gm/"

    mins_file = path + "/mins.p"
    maxes_file = path + "/maxes.p"

    model = GaussianMixtureModel.load(model_folder)
    gm = GaussianMixture.load(gm_folder)
    mins = pickle.load(open(mins_file, "rb"))
    maxes = pickle.load(open(maxes_file, "rb"))

    return model, gm.getK(), mins, maxes


def map_to_result_df(df, header, labels_count):
    columns = list()
    for index in range(0, labels_count):
        column_name = header[index]
        df_column_name = "_" + str(index + 1)
        column = df.labels[df_column_name].alias(column_name)
        columns.append(column)

    columns.append(F.col("anomaly_probability"))
    resultDF = df.select(columns)
    return resultDF


def predict(spark, input_path, model_path, labels_count=6):
    model, kernels, mins, maxes = load(model_path)

    preparator = DataPreparator(spark)
    initRDD, header = preparator.aggregate_parquet_and_map(input_path, labels_count)
    normalizedRDD = preparator.normalize(initRDD, mins, maxes)
    testDataset = preparator.to_train_df(normalizedRDD)

    predicted = model.transform(testDataset)
    predicted = preparator.add_anomaly_probability(predicted, kernels)

    resultDF = map_to_result_df(predicted, header, labels_count)
    resultPD = resultDF.toPandas()
    result_array = resultPD.to_records()

    columns = list(header[:labels_count])
    columns.append("anomaly_probability")

    # Prediction = namedtuple('Prediction', resultPD.dtypes.index.tolist())
    # result_array = list()
    # for index, row in enumerate(resultPD):
    #     prediction = Prediction(*resultPD.iloc[index,:])
    #     result_array.append(prediction)

    return result_array, columns


def predict_streaming(spark, rdd, model_path, labels_count=6):
    model, kernels, mins, maxes = load(model_path)
    preparator = DataPreparator(spark)

    # TODO: Somehow get header from PubSub
    header = 'pickup_date,pickup_hour,pickup_taxizone_id,dropoff_date,dropoff_hour,dropoff_taxizone_id,total_amount,trip_distance,passenger_count,tip_amount,trip_count'
    header = header.split(',')

    testDataset = preparator.prepare_streaming_data(rdd, labels_count, mins, maxes)
    predicted = model.transform(testDataset)

    predicted = preparator.add_anomaly_probability(predicted, kernels)

    resultDF = map_to_result_df(predicted, header, labels_count)
    resultPD = resultDF.toPandas()
    result_array = resultPD.to_records()

    columns = list(header[:labels_count])
    columns.append("anomaly_probability")

    return result_array, columns


def predict_streaming_test(spark, rdd, model_path, labels_count=6):
    model, kernels, mins, maxes = load(model_path)
    preparator = DataPreparator(spark)

    # TODO: Somehow get header from Kafka?
    header = ['pickup_date', 'pickup_hour', 'pickup_taxizone_id', 'dropoff_date', 'dropoff_hour', 'dropoff_taxizone_id',
              'total_amount', 'trip_distance', 'passenger_count', 'tip_amount', 'trip_count']

    testDataset = preparator.prepare_streaming_data(rdd, labels_count, mins, maxes)
    # testDataset.show()

    predicted = model.transform(testDataset)

    predicted = preparator.add_anomaly_probability(predicted, kernels)

    resultDF = map_to_result_df(predicted, header, labels_count)
    # resultDF.show()

    return resultDF


# resultPD = resultDF.toPandas()
# result_array = resultPD.to_records()

# columns = list(header[:labels_count])
# columns.append("anomaly_probability")

# return result_array, columns


if __name__ == "__main__":

    parser = ArgumentParser(description='Predicting anomaly probability.')
    parser.add_argument('--input_path', type=str, help='Path to raw input dataset')
    parser.add_argument('--labels_count', type=int,
                        help='Amount of "dimensions" - columns that will not infuence the prediction process',
                        default=6)
    parser.add_argument('--model_path', type=str, help='Path to directory where trained model is stored')
    args = parser.parse_args()

    app_name = 'Taxi Anomaly Detection'
    spark = SparkSession \
        .builder \
        .appName(app_name) \
        .getOrCreate()

    result_array = predict(spark, args.input_path, args.model_path, args.labels_count)

    for predicted in result_array:
        print(predicted)
