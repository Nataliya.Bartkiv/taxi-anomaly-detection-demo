from __future__ import print_function, absolute_import

from pyspark.streaming.kafka import KafkaUtils
from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
from pyspark.sql import SparkSession
import pyspark.sql.functions as F

import pprint
import subprocess
from argparse import ArgumentParser

import time as time_lib
import datetime

import sys


# To run on the cluster:
sys.path.append('/shared/taxi-anomaly-detection-demo/python/taxi-anomaly-detection/src/datascience')

# # To run locally:
# sys.path.append('../datascience')

import predict as P


def getSparkSessionInstance(sparkConf):
    if 'sparkSessionSingletonInstance' not in globals():
        globals()['sparkSessionSingletonInstance'] = SparkSession \
            .builder \
            .config(conf=sparkConf) \
            .getOrCreate()
    return globals()['sparkSessionSingletonInstance']


if __name__ == "__main__":
    TOPICS_DEFAULT = 'taxi-aggregated'
    BROKERS_DEFAULT = 'dataproc-cluster-w-0:9092,dataproc-cluster-w-1:9092'
    MASTER_DEFAULT = 'yarn'
    MODEL_PATH_DEFAULT = '/shared/taxi/models/gmm_model/'
    BIGQUERY_DATASET_DEFAULT = 'taxi_anomaly_detection_dataset'
    BIGQUERY_TABLE_DEFAULT = 'predictions'
    BATCH_DURATION_DEFAULT = 300

    parser = ArgumentParser(description='Reading taxi data from Kafka, predict anomalies and save result to BigQuery')
    parser.add_argument('--topics', type=str, default=TOPICS_DEFAULT,
                        help='Comma-separated Kafka topics list to read messages from.')
    parser.add_argument('--bootstrap_servers', type=str, default=BROKERS_DEFAULT,
                        help='Comma-separated list of Kafka brokers: <host>:<port>,<host>:<port>')
    parser.add_argument('--master', type=str, default=MASTER_DEFAULT,
                        help='Spark master URL')
    parser.add_argument('--model_path', type=str, default=MODEL_PATH_DEFAULT,
                        help='Path to trained model with metadata (folder must be located both on HDFS and locally.')
    parser.add_argument('--big_query_dataset', type=str, default=BIGQUERY_DATASET_DEFAULT,
                        help='Dataset in BigQuery for storing results.')
    parser.add_argument('--big_query_table', type=str, default=BIGQUERY_TABLE_DEFAULT,
                        help='BigQuery table for storing results.')
    parser.add_argument("--batch_duration", type=int, default=BATCH_DURATION_DEFAULT,
                        help='the time interval (in seconds) at which streaming data will be wirtten to BiqQuery')

    args = parser.parse_args()

    app_name = 'Streaming Predictions'
    topics = args.topics.split(',')
    brokers = args.bootstrap_servers
    master = args.master
    model_path = args.model_path
    output_dataset = args.big_query_dataset
    output_table = args.big_query_table
    batch_duration = args.batch_duration

    # Create spark context
    conf = SparkConf().setAppName(app_name).setMaster(master)
    sc = SparkContext(conf=conf)
    ssc = StreamingContext(sc, batch_duration)

    # GCP configurations
    bucket = sc._jsc.hadoopConfiguration().get('fs.gs.system.bucket')
    project = sc._jsc.hadoopConfiguration().get('fs.gs.project.id')

    # Minimize logs
    logger = sc._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.ERROR)
    logger.LogManager.getLogger("akka").setLevel(logger.Level.ERROR)

    # Create DStream from Kafka topic
    stream = KafkaUtils.createDirectStream(ssc, topics, {"metadata.broker.list": brokers})

    def process(time, rdd):
        print("========= %s =========" % str(time))

        # TODO: Do not mock column names here
        column_to_drop = 'dropoff_hour'
        column_to_add = 'dropoff_window'

        spark = getSparkSessionInstance(rdd.context.getConf())

        if rdd.isEmpty():
            return

        # Get time window as string
        now = datetime.datetime.utcnow()
        before = now - datetime.timedelta(seconds=batch_duration)
        time_format = '%H:%M:%S'
        now_str = now.strftime(time_format)
        before_str = before.strftime(time_format)
        window_str = before_str + ' - ' + now_str


        rdd = rdd.map(lambda value: value[1])
        df = P.predict_streaming_test(spark, rdd, model_path)

        headers = df.columns
        column_to_drop_index = headers.index(column_to_drop)
        headers[column_to_drop_index] = column_to_add

        df = df.withColumn(column_to_add, F.lit(window_str))
        df = df.select(headers)

        df.show()

        timestamp_str = str(int(time_lib.time() * 1000))
        output_directory = 'gs://{0}/hadoop/tmp/bigquery/pyspark_output_{1}'.format(bucket, timestamp_str)

        # Save predicted results to BigQuery
        writeToBQ(spark, df, project, output_dataset, output_table, output_directory)


    def writeToBQ(spark, df, project_id, output_dataset, output_table, output_directory):
        # Display 10 results.
        pprint.pprint(df.take(10))

        # Stage data formatted as JSON in Cloud Storage.
        df.write.format('json').save(output_directory)

        # Shell out to bq CLI to perform BigQuery import.
        output_files = output_directory + '/part-*'
        subprocess.check_call(
            'bq load --source_format NEWLINE_DELIMITED_JSON '
            '--autodetect '
            '--project_id {project_id} '
            '{dataset}.{table} {files}'.format(
                dataset=output_dataset, table=output_table, files=output_files,
                project_id=project_id
            ).split())

        # Manually clean up the staging_directories, otherwise BigQuery
        # files will remain indefinitely.
        output_path = sc._jvm.org.apache.hadoop.fs.Path(output_directory)
        output_path.getFileSystem(sc._jsc.hadoopConfiguration()).delete(
            output_path, True)


    stream.foreachRDD(process)

    ssc.start()
    ssc.awaitTermination()

# spark-submit --jars spark-streaming-kafka-0-8-assembly_2.11-2.2.0.jar streaming_predictions.py
