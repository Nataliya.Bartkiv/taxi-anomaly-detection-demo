$(document).ready(function(){
    //TODO: Add disconnect call

    function formatDate(date) {
        var year = date.getFullYear();
        var month = ("0" + date.getMonth()).substr(-2);
        var day = ("0" + date.getDate()).substr(-2);
        var formatted_date = year + "-" + month + "-" + day
        return formatted_date
    }

    function fillTable(tableId, data, headers, rowCount) {

        var tableHtml = '<thead><tr>';
        for(var headerIndex = 0; headerIndex < headers.length; ++headerIndex) {
            tableHtml += '<th>' + headers[headerIndex] + '</th>';
        }
        tableHtml += '</tr></thead><tfoot>'

        for(var headerIndex = 0; headerIndex < headers.length; ++headerIndex) {
            tableHtml += '<th>' + headers[headerIndex] + '</th>';
        }
        tableHtml += '</tr></tfoot><tbody>'

        for(var rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
            tableHtml += '<tr>'
            for(var headerIndex = 0; headerIndex < headers.length; ++headerIndex) {
                var header = headers[headerIndex];
                var value = data[header][rowIndex];
                if(header.endsWith("_date")) {
                    var date = new Date(value);
                    value = formatDate(date)
                }

                tableHtml += '<td>' + value + '</td>';
            }
            tableHtml += '</tr>'
        }
        tableHtml += '</tbody>';

        $('#' + tableId).html(tableHtml);
    }

    function selectAnomalies(data, threshold, headers, rowCount) {
        var selected = {};
        for(var i = 0; i < rowCount; ++i) {
            selected[headers[i]] = [];
        }

        for(var i = 0; i < rowCount; ++i) {
            if(data["anomaly_probability"][i] > threshold) {
                for(var j = 0; j < headers.length; ++j) {
                    selected[headers[j]].push(data[headers[j]][i])
                }
            }
        }
        return selected
    }


    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/test');

    socket.on('newdata', function(data) {
        var WARNING_PROBABILITY = 0.3
        var DANGER_PROBABILITY = 0.6

        var tableId = "streaming_result_tbl"
        var anomalousTableId = "streaming_result_tbl_anomalous"

        var parsedData = JSON.parse(data);
        var headers = Object.keys(parsedData);
        var rowCount = Object.keys(parsedData[headers[0]]).length

        fillTable(tableId, parsedData, headers, rowCount)
        setCheckMarks(tableId, WARNING_PROBABILITY, DANGER_PROBABILITY);
        $('#' + tableId).dataTable({
             destroy: true,
             searching: true,
             ordering:  true,
             lengthMenu: [10, 25, 50, 100 ]
        });

        var anomalousData = selectAnomalies(parsedData, WARNING_PROBABILITY, headers, rowCount)
        var anomalousRowCount = Object.keys(anomalousData[headers[0]]).length
        fillTable(anomalousTableId, anomalousData, headers, anomalousRowCount)
        setCheckMarks(anomalousTableId, WARNING_PROBABILITY, DANGER_PROBABILITY);
        $('#' + anomalousTableId).dataTable({
             destroy: true,
             searching: true,
             ordering:  true,
             lengthMenu: [10, 25, 50, 100 ]
        });
    });

    socket.on('taxizones', function(data) {
        var parsedData = JSON.parse(data);

        var selectHtml = "";

        for(var i = 0; i < parsedData.length; ++i) {
            var optionHtml = "<option>" + parsedData[i] + "</option>\n";
            selectHtml += optionHtml;
        }

        $('#pickup-taxizone').html(selectHtml);
        $('#dropoff-taxizone').html(selectHtml);
    });

    socket.on('linechartdata', function(labels, datasets) {
        createLineChart("line-chart", labels, datasets)
    });

    function getLineChartData() {
//        var pickupTaxizone = $("#pickup-taxizone option:selected").text();
//        var dropoffTaxizone = $("#dropoff-taxizone option:selected").text();
//        var date = "2018-05-17"
//
//        var parameters = {
//            pickup_taxizone: pickupTaxizone,
//            dropoff_taxizone: dropoffTaxizone,
//            date: date
//        }
//        var json_parameters = JSON.stringify(parameters)
//
//        socket.emit('get_linechart_data', parameters)
        socket.emit('get_linechart_data')
    }

     getLineChartData();
//     $('#pickup-taxizone').change(getLineChartData);
//     $('#dropoff-taxizone').change(getLineChartData);

});

