var darkTheme = {
    backgroundColor: '#1F1F1F',
    lineBorderColor: '#6ED1E1',
    lineBackgroundColor : 'rgba(110, 209, 225, 0.5)',
    warningBorderColor: '#ffc107',
    warningBackgroundColor: 'rgba(255, 193, 7, 0.5)',
    dangerBorderColor: '#dc3545',
    dangerBackgroundColor: 'rgba(220, 53, 69, 0.5)',
    gridLineColor: 'rgba(255, 255, 255, 0.3)'
};

var lightTheme = {
    backgroundColor: 'white',
    lineBorderColor: '#6ED1E1',
    lineBackgroundColor : 'rgba(110, 209, 225, 0.5)',
    warningBorderColor: '#ffc107',
    warningBackgroundColor: 'rgba(255, 220, 7, 0.5)',
    dangerBorderColor: '#dc3545',
    dangerBackgroundColor: 'rgba(220, 53, 69, 0.5)',
    gridLineColor: 'rgba(0, 0, 0, 0.3)'
};


function getRandomColor() {
    var red = Math.floor(Math.random() * 255)
    var green = Math.floor(Math.random() * 255);
    var blue = Math.floor(Math.random() * 255);
    border = "rgb(" + red + ", " + green + ", " + blue + ")"
    background = "rgba(" + red + ", " + green + ", " + blue + ", 0.3)"
    return { "border" : border, "background" : background }
}

function parseRawData(raw_datasets, theme) {
    datasets = [];

    for(var taxizone in raw_datasets) {
        if(raw_datasets.hasOwnProperty(taxizone)) {
            var color = getRandomColor()

            dataset = {
                label: "Taxizone # " + taxizone,
                data: raw_datasets[taxizone],
                borderColor: color.border,
                backgroundColor: color.background,
                fill: true,
                pointBackgroundColor: color.border,
                pointBorderColor: color.border,

//                //With fill
//                backgroundColor: color.background,
//                fill: true

                //Without fill
                fill: false
            }

            datasets.push(dataset)
        }
    }

    return datasets

}

function addThresholdLines(datasets, labels, theme) {
    function getFilledArray(length, value) {
        var array = [];
        for(var i = 0; i < length; ++i) {
            array.push(value)
        }
        return array;
    }

    var labelLength = labels.length;
    var warningValue = 0.7;
    var dangerValue = 0.9;
    var topValue = 1.0;

    var warningsBottom = getFilledArray(labelLength, warningValue)
    var dangersBottom = getFilledArray(labelLength, dangerValue)
    var dangersTop = getFilledArray(labelLength, topValue)

    datasets.push({
        label: "",
        data: dangersBottom,
        borderColor: theme.dangerBorderColor,
        backgroundColor: theme.dangerBackgroundColor,
        radius: 0,
        pointHoverRadius: 0,
        fill: 'end'
    })

    datasets.push({
        label: "",
        data: warningsBottom,
        borderColor: theme.warningBorderColor ,
        backgroundColor: theme.warningBackgroundColor,
        radius: 0,
        pointHoverRadius: 0
    })

    datasets.push({
        label: "",
        data: dangersTop,
        radius: 0,
        pointHoverRadius: 0
    })


    return datasets
}

function createLineChart(elementId, labels, raw_datasets) {
    // Change to 'darkTheme' or 'lightTheme' to change chart color palette
    var theme = lightTheme;

    // For mock data:

    // raw_datasets = JSON.parse(raw_datasets)

    var datasets = parseRawData(raw_datasets, theme)
    datasets = addThresholdLines(datasets, labels, theme)

    var data = {
        labels: labels,
        datasets: datasets,
    };

    var options = {
        title: {
            display: false,
            text: 'Anomaly probability per hour'
        },
        legend: {
            display: false
        },
        layout: {
            padding: { left: 20, right: 20, top: 20, bottom: 0 }
        },
        elements: {
            line: {
                tension: 0,
                fill: '-1',
                borderWidth: 3
            },
            point: {
                radius: 3
            }
        },
        scales: {
            xAxes: [{
                gridLines: {
                    color: theme.gridLineColor,
                    display: true
                }
            }],
            yAxes: [{
                gridLines: {
                    color: theme.gridLineColor,
                    display: true
                }
            }]
        }
    };

    var ctx = document.getElementById(elementId)

    var multilineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: options
    });

    ctx.style.backgroundColor = theme.backgroundColor;
}



