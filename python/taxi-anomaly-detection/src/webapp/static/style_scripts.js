function setCheckMarks(table_id, warning_probability = 0.3, danger_probability = 0.5)
{
//	var table_id = "result_tbl";
//	var warning_probability = 0.3;
//	var danger_probability = 0.6;
	var anomaly_prob_col_index = 6;

	var table = document.getElementById(table_id);
	var rowNumber = table.rows.length;
    for (var i = 0; i < rowNumber; i++)
    {
    	var cell = table.rows[i].cells[anomaly_prob_col_index]
    	var anomaly_prob = parseFloat(cell.innerText);
    	if(anomaly_prob >= danger_probability)
    	{
            table.rows[i].className += " bg-danger"
    	} else if (anomaly_prob >= warning_probability) {
            table.rows[i].className += " bg-warning"
    	}
	}
};

function styleFileUpload(){
    document.querySelector("html").classList.add('js');

    var fileInput  = document.querySelector( "#my-file" ),
    button     = document.querySelector( "#my-file" ),
    the_return = document.querySelector("#filename");

    button.addEventListener( "keydown", function( event ) {
        if ( event.keyCode == 13 || event.keyCode == 32 ) {
            fileInput.focus();
        }
    });
    button.addEventListener( "click", function( event ) {
       fileInput.focus();
       return false;
    });
    fileInput.addEventListener( "change", function( event ) {
        the_return.innerHTML = this.value;
    });
};

