spark-submit \
--master yarn \
--deploy-mode cluster \
--queue default \
--num-executors 6 \
--executor-memory 1G \
/home/natalie/Documents/DeliveryDemo/taxi-anomaly-detection/src/train.py \
	--input_path /home/natalie/Documents/DeliveryDemo/taxi-anomalies-sparkml/agg-data-subset/ \
	--labels_count 6 \
	--kernels 3 \
	--seed 538009335 \
	--model_path /home/natalie/Documents/DeliveryDemo/taxi-anomaly-detection/models/gmm_model_job



spark-submit \
/home/natalie/Documents/DeliveryDemo/taxi-anomaly-detection/src/predict.py \
	--input_path  /home/natalie/Documents/DeliveryDemo/taxi-anomalies-sparkml/raw-test-data-subset \
	--model_path /home/natalie/Documents/DeliveryDemo/taxi-anomaly-detection/models/gmm_model_job


spark-submit \
--executor-memory 2G \
/home/natalie/Documents/DeliveryDemo/taxi-anomaly-detection/src/k-means-silhouette.py \
	--input_path /home/natalie/Documents/DeliveryDemo/taxi-anomalies-sparkml/agg-data-subset/ \
	--labels_count 6


# Cluster run

# Train
spark-submit \
--master yarn \
--deploy-mode cluster \
--queue default \
--num-executors 6 \
--executor-memory 2G \
--py-files /shared/taxi/src/data_aggregation.py,/shared/taxi/src/data_preparation.py \
--conf spark.yarn.appMasterEnv.PYTHON_ROOT=/opt/conda/bin/python \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=/opt/conda/bin/python \
--conf spark.yarn.appMasterEnv.SPARK_YARN_USER_ENV="PYSPARK_PYTHON=/opt/conda/bin/python" \
--conf spark.yarn.executorEnv.LD_LIBRARY_PATH=/lib64 \
/shared/taxi/src/train.py \
--input_path hdfs://dataproc-cluster-m/shared/NYT_smaller_aggregated \
--labels_count 6 \
--kernels 3 \
--seed 538009335 \
--model_path /shared/taxi/models/gmm_model


# Aggregate
spark-submit \
--master yarn \
--deploy-mode cluster \
--queue default \
--num-executors 6 \
--executor-memory 1G \
/shared/taxi/src/data_aggregation.py \
	--input_path hdfs://dataproc-cluster-m/shared/NYT_smaller \
	--output_path hdfs://dataproc-cluster-m/shared/NYT_smaller_aggregated


# Prepare features

spark-submit \
--master yarn \
--deploy-mode cluster \
--queue default \
--num-executors 6 \
--executor-memory 2G \
--conf spark.yarn.appMasterEnv.PYTHON_ROOT=/opt/conda/bin/python \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=/opt/conda/bin/python \
--conf spark.yarn.appMasterEnv.SPARK_YARN_USER_ENV="PYSPARK_PYTHON=/opt/conda/bin/python" \
--conf spark.yarn.executorEnv.LD_LIBRARY_PATH=/lib64 \
/shared/taxi/src/prepare_features.py \
--input_path /NYT \
--output_path /shared/taxi/data/features \
--columns total_amount,tip_amount,trip_distance,passenger_count \
--with_trip_duration

