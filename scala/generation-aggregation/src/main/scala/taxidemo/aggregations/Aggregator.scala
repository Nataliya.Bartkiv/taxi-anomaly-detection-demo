package taxidemo.aggregations

import org.apache.beam.runners.dataflow.DataflowRunner
import org.apache.beam.sdk.Pipeline
import org.apache.beam.sdk.coders.StringUtf8Coder
import org.apache.beam.sdk.io.kafka.{KafkaIO, KafkaRecord}
import org.apache.beam.sdk.options.PipelineOptionsFactory
import org.apache.beam.sdk.transforms.windowing.{FixedWindows, Window}
import org.apache.beam.sdk.transforms._
import org.apache.beam.sdk.values.KV
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.joda.time.Duration
import taxidemo.aggregations.utils._
import taxidemo.entities._


object Aggregator {
    val KAFKA_BOOTSTRAP_SERVERS_DEV = "localhost:9093,localhost:9094"
    val WINDOW_DURATION_SECONDS_DEV = 5

    val KAFKA_BOOTSTRAP_SERVERS = "dataproc-cluster-w-0:9092"
    val KAFKA_INPUT_TOPIC = "taxi-trips"
    val KAFKA_OUTPUT_TOPIC = "taxi-aggregated"
    val WINDOW_DURATION_SECONDS = 3600

    val PROJECT_NAME = "tfos-anomaly-detection"
    val REGION = "europe-west1"

    def main(args: Array[String]): Unit = {
        val options = PipelineOptionsFactory
            .fromArgs(args: _*).withValidation()
            .as(classOf[TaxiAggregationOptions])

//        options.setRunner(classOf[DataflowRunner])
        val pipeline = Pipeline.create(options)

        val aggregatedData = pipeline
            .begin()
            .apply("Read Lines From Kafka Topic", KafkaIO.read()
                .withBootstrapServers(options.getBootstrapServers)
                .withTopic(options.getInputTopic)
                .withKeyDeserializerAndCoder(classOf[StringDeserializer], StringUtf8Coder.of)
                .withValueDeserializerAndCoder(classOf[StringDeserializer], StringUtf8Coder.of))
            .apply("Get Values From Kafka Lines", MapElements.via(new SimpleFunction[KafkaRecord[String, String], String]() {
                override def apply(input: KafkaRecord[String, String]): String = {
//                    println(input.getKV.getValue)
                    input.getKV.getValue
                }
            }))
            .apply("Parse TaxiTrip From String", MapElements.via(new ParseTaxiTrip))
            .apply("Set Timestamp", ParDo.of(new AddTimestamp))
            .apply("Set Window", Window.into[TaxiTrip](FixedWindows.of(Duration.standardSeconds(/* WINDOW_DURATION_SECONDS_DEV */ options.getWindowDuration))))
            .apply("Map Lines To Labels/Features", MapElements.via(new MapToLabelsFeatures))
            .apply("Sum Up Taxi Features", Combine.perKey(new AggregateFeatures))
            .apply("Intermediate Mapping", MapElements.via(new  SimpleFunction[KV[String, TaxiFeatures], KV[String, String]]() {
            override def apply(input: KV[String, TaxiFeatures]): KV[String, String] = {
                val key = input.getKey
                val value = key + "," + input.getValue.toString
                KV.of(key, value)
            }
        }))
            .apply("Write Aggregated Data to Kafka Topic", KafkaIO.write()
                .withBootstrapServers(options.getBootstrapServers)
                .withTopic(options.getOutputTopic)
                .withValueSerializer(classOf[StringSerializer])
                .withKeySerializer(classOf[StringSerializer])
            )

        pipeline.run().waitUntilFinish()
    }
}
