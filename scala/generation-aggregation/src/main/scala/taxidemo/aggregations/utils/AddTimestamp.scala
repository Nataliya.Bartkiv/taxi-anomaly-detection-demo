package taxidemo.aggregations.utils

import org.apache.beam.sdk.transforms.DoFn
import org.apache.beam.sdk.transforms.DoFn.ProcessElement
import org.joda.time.Instant
import taxidemo.entities.TaxiTrip

class AddTimestamp extends DoFn[TaxiTrip, TaxiTrip] {
    @ProcessElement
    def processElement(c : ProcessContext) : Unit = {
        //        c.outputWithTimestamp(c.element(), new Instant(System.currentTimeMillis()))
        c.outputWithTimestamp(c.element(), new Instant(c.timestamp()))
    }
}
