package taxidemo.aggregations.utils

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.beam.sdk.transforms.SimpleFunction
import org.apache.beam.sdk.values.KV
import taxidemo.entities._

class MapToLabelsFeatures extends SimpleFunction[TaxiTrip, KV[String, TaxiFeatures]] {
    override def apply(input: TaxiTrip): KV[String, TaxiFeatures] = {
        val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
        val hourFormat = new SimpleDateFormat("HH")

        val dropoff_datetime: Date = new Date(input.dropoff_datetime)
        val dropoff_date = dateFormat.format(dropoff_datetime)
        val dropoff_hour = hourFormat.format(dropoff_datetime)

        val pickup_datetime: Date = new Date(input.pickup_datetime)
        val pickup_date = dateFormat.format(pickup_datetime)
        val pickup_hour = hourFormat.format(pickup_datetime)

        val labels = TaxiLabels(dropoff_date, dropoff_hour, input.dropoff_taxizone_id,
            pickup_date, pickup_hour, input.pickup_taxizone_id)

        val features = TaxiFeatures(1, input.total_amount,
            input.passenger_count, input.tip_amount, input.trip_distance)

        KV.of(labels.toString, features)
    }
}
