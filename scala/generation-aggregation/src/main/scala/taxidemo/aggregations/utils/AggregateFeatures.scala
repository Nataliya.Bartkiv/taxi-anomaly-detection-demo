package taxidemo.aggregations.utils

import java.lang

import org.apache.beam.sdk.transforms.Combine.CombineFn
import taxidemo.entities.TaxiFeatures

import collection.JavaConversions._

class AggregateFeatures extends CombineFn[TaxiFeatures, TaxiFeatures, TaxiFeatures] {
    override def createAccumulator(): TaxiFeatures = TaxiFeatures()

    override def addInput(accumulator: TaxiFeatures, input: TaxiFeatures): TaxiFeatures = {
        TaxiFeatures(
            accumulator.trip_count + input.trip_count,
            accumulator.total_amount + input.total_amount,
            accumulator.passenger_count + input.passenger_count,
            accumulator.tip_amount + input.tip_amount,
            accumulator.trip_distance + input.trip_distance
        )
    }

    override def mergeAccumulators(accumulators: lang.Iterable[TaxiFeatures]): TaxiFeatures = {
        var trip_count: Long = 0L
        var total_amount: Float = 0F
        var passenger_count: Long = 0L
        var tip_amount: Float = 0F
        var trip_distance: Float = 0F

        for (accumulator: TaxiFeatures <- accumulators) {
            trip_count += accumulator.trip_count
            total_amount += accumulator.total_amount
            passenger_count += accumulator.passenger_count
            tip_amount += accumulator.tip_amount
            trip_distance += accumulator.trip_distance
        }

        TaxiFeatures(trip_count, total_amount, passenger_count, tip_amount, trip_distance)
    }

    override def extractOutput(accumulator: TaxiFeatures): TaxiFeatures = {
        println(accumulator)
        accumulator
    }
}
