package taxidemo.aggregations.utils

import org.apache.beam.sdk.transforms.SimpleFunction
import taxidemo.entities.TaxiTrip

class ParseTaxiTrip extends SimpleFunction[String, TaxiTrip] {
    override def apply(input: String): TaxiTrip = {
        val taxiTrip = TaxiTrip()
        taxiTrip.init(input)
        taxiTrip
    }
}
