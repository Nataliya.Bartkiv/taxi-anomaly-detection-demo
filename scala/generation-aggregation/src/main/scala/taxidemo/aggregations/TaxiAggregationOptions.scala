package taxidemo.aggregations

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions
import org.apache.beam.sdk.options.{Default, Description}

trait TaxiAggregationOptions extends DataflowPipelineOptions {
    @Description("Input Kafka topic with taxi trip messages.")
    @Default.String("taxi-trips")
    def getInputTopic : String

    def setInputTopic(value : String)


    @Description("Output Kafka topic for aggregated taxi messages.")
    @Default.String("taxi-aggregated")
    def getOutputTopic : String

    def setOutputTopic(value : String)


    @Description("Comma-separated list of Kafka brokers (<host>:<port>,<host>:<port>,...)")
    @Default.String("dataproc-cluster-w-0:9092,dataproc-cluster-w-1:9092")
    def getBootstrapServers : String

    def setBootstrapServers(value : String)


    @Description("Window duration (in seconds)")
    @Default.Integer(3600)
    def getWindowDuration : Int

    def setWindowDuration(value : Int)
}
