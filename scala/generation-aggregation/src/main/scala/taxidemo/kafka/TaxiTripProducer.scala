package taxidemo.kafka

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, Producer, ProducerRecord}
import taxidemo.generator.TaxiTripGenerator

class TaxiTripProducer (topic : String, bootstrapServers : String) {
    val properties : Properties = new Properties()

    def defaultInit(): Unit = {
        properties.put("bootstrap.servers", bootstrapServers)
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    }

    def nextRecord() : ProducerRecord[String, String] = {
        val trip = TaxiTripGenerator.next()

        val key = s"${trip.pickup_datetime}-${trip.dropoff_taxizone_id}"
        val value = trip.toString
        println(s"Producing next value: $value")
        new ProducerRecord[String, String](topic, key, value)
    }

    def produce(messageCount : Long, messageInterval : Long) : Unit = {

        val producer: Producer[String, String] = new KafkaProducer(properties)
        for(_ <- 0L until messageCount) {
            val record = nextRecord()

            producer.send(record)
            Thread.sleep(messageInterval)
        }

        producer.close()
    }
}

object TaxiTripProducer {
    // Default values
    val MESSAGE_COUNT = 100000
    val MESSAGE_INTERVAL = 200
    val TOPIC = "taxi-trips"
    val BOOTSTRAP_SERVERS = "dataproc-cluster-w-0:9092,dataproc-cluster-w-1:9092"
    val BOOTSTRAP_SERVERS_DEV = "localhost:9093"

    val usage =
        """
    Usage: java -jar <jar-name> \
    --bootstrap-servers <comma-delimited list of Kafka brokers> \
    --topic <Kafka topic to produce messages to> \
    --message-count <count of messages to produce> \
    --message-interval-ms <interval between producing messages in ms>
  """

    type OptionMap = Map[Symbol, Any]

    def main(args: Array[String]) {
        if (args.length == 1 && args(0) == "--help") {
            println(usage)
            System.exit(0)
        }
        val arglist = args.toList

        val options = nextOption(Map(), arglist)
        if (options == null) {
            System.exit(1)
        }

        val topic = if (options.contains('topic)) options('topic).toString else TOPIC
        val bootstrapServers = if (options.contains('bootstrapServers)) options('bootstrapServers).toString else BOOTSTRAP_SERVERS
        val messageCount = if (options.contains('messageCount)) options('messageCount).asInstanceOf[Long] else MESSAGE_COUNT
        val messageInterval = if (options.contains('messageInterval)) options('messageInterval).asInstanceOf[Long] else MESSAGE_INTERVAL

        val producer = new TaxiTripProducer(topic, bootstrapServers)
        producer.defaultInit()
        producer.produce(messageCount, messageInterval)
    }

    def nextOption(map: OptionMap, list: List[String]): OptionMap = {
        list match {
            case Nil => map
            case "--bootstrap-servers" :: value :: tail =>
                nextOption(map ++ Map('bootstrapServers -> value.toString), tail)
            case "--topic" :: value :: tail =>
                nextOption(map ++ Map('topic -> value.toString), tail)
            case "--message-count" :: value :: tail =>
                nextOption(map ++ Map('messageCount -> value.toLong), tail)
            case "--message-interval-ms" :: value :: tail =>
                nextOption(map ++ Map('messageInterval -> value.toLong), tail)
            case option :: tail => println("Unknown option " + option); null
        }
    }
}
