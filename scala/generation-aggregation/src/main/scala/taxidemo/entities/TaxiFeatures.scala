package taxidemo.entities

case class TaxiFeatures(trip_count : Long = 0L,
                        total_amount : Float = 0F,
                        passenger_count : Long = 0L,
                        tip_amount : Float = 0F,
                        trip_distance : Float = 0F) {
    def this() {
        this(0)
    }

    override def toString: String = {
        s"$total_amount,$trip_distance,$passenger_count,$tip_amount,$trip_count"
    }
}
