package taxidemo.entities

case class Label (name : String,
                  values : List[String],
                  frequencies : List[Double])
