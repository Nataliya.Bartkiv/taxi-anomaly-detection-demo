package taxidemo.entities

case class TaxiTrip(var dropoff_datetime : Long = 0L,
                    var dropoff_latitude : Float = 0F,
                    var dropoff_longitude : Float = 0F,
                    var dropoff_taxizone_id : Int = 0,
                    var ehail_fee : Float = 0F,
                    var extra : Float = 0F,
                    var fare_amount : Float = 0F,
                    var improvement_surcharge : Float = 0F,
                    var mta_tax : Float = 0F,
                    var passenger_count : Int = 0,
                    var payment_type : String = "",
                    var pickup_datetime : Long = 0L,
                    var pickup_latitude : Float = 0L,
                    var pickup_longitude : Float = 0L,
                    var pickup_taxizone_id : Int = 0,
                    var  rate_code_id : Int = 0,
                    var store_and_fwd_flag : String = "",
                    var tip_amount : Float = 0F,
                    var tolls_amount : Float = 0F,
                    var total_amount : Float = 0F,
                    var trip_distance : Float = 0F,
                    var trip_type : String = "",
                    var vendor_id : String = "",
                    var trip_id : Long = 0L
                   ) {
    def this() {
        this(0L)
    }

    def init(line : String) {
        val start = line.indexOf("(") + 1
        val end = line.lastIndexOf(")")
        val valuesLine = line.substring(start, end)
        val tokens = valuesLine.split(",")

        this.dropoff_datetime = tokens(0).toLong
        this.dropoff_latitude = tokens(1).toFloat
        this.dropoff_longitude = tokens(2).toFloat
        this.dropoff_taxizone_id = tokens(3).toInt
        this.ehail_fee = tokens(4).toFloat
        this.extra = tokens(5).toFloat
        this.fare_amount = tokens(6).toFloat
        this.improvement_surcharge = tokens(7).toFloat
        this.mta_tax = tokens(8).toFloat
        this.passenger_count = tokens(9).toInt
        this.payment_type = tokens(10)
        this.pickup_datetime = tokens(11).toLong
        this.pickup_latitude = tokens(12).toFloat
        this.pickup_longitude = tokens(13).toFloat
        this.pickup_taxizone_id = tokens(14).toInt
        this.rate_code_id = tokens(15).toInt
        this.store_and_fwd_flag = tokens(16)
        this.tip_amount = tokens(17).toFloat
        this.tolls_amount = tokens(18).toFloat
        this.total_amount = tokens(19).toFloat
        this.trip_distance = tokens(20).toFloat
        this.trip_type = tokens(21)
        this.vendor_id = tokens(22)
        this.trip_id = tokens(23).toLong
    }

}
