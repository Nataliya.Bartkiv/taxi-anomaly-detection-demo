package taxidemo.entities

case class TaxiLabels (dropoff_date : String = "",
                       dropoff_hour : String = "",
                       dropoff_taxizone_id : Int = 0,
                       pickup_date : String = "",
                       pickup_hour : String = "",
                       pickup_taxizone_id : Int = 0) {
    def this() {
        this("")
    }

    override def toString: String = {
        s"$pickup_date,$pickup_hour,$pickup_taxizone_id,$dropoff_date,$dropoff_hour,$dropoff_taxizone_id"
    }
}

