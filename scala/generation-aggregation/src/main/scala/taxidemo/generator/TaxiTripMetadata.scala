package taxidemo.generator

import java.io.File

import taxidemo.entities.{Label, Feature}

import scala.collection.mutable
import scala.io.Source

object TaxiTripMetadata {
    val labelsDataPath = "taxi-labels"
    val featuresMetadata = "taxi-features"
    val labels: mutable.HashMap[String, Label] = new mutable.HashMap[String, Label]
    val features: mutable.HashMap[String, Feature] = new mutable.HashMap[String, Feature]

    {
        loadLabelsMetadata()
        loadFeaturesMetadata()
    }

    def loadFeaturesMetadata() : Unit = {
        val files = getCsvFiles(featuresMetadata)
        for (file: File <- files) {
            for (line <- Source.fromFile(file).getLines) {
                val tokens = line.split(',')

                val name = tokens(0)
                val average = parseDouble(tokens(1))
                val variance = parseDouble(tokens(2))

                val feature = Feature(name, average, variance)

                features.update(name, feature)
            }
        }
    }


    def loadLabelsMetadata() : Unit = {
        val metadataDir = new File(labelsDataPath)
        val subdirs = metadataDir.listFiles
            .filter(_.isDirectory)
            .map(_.getPath)
            .toList

        for (subdir : String <- subdirs) {
            val label = readLabel(subdir)
            labels.update(label.name, label)
        }
    }

    def readLabel(inputPath : String) : Label = {
        val name = extractName(inputPath)
        val values = mutable.Buffer[String]()
        val frequencies = mutable.Buffer[Double]()

        val files = getCsvFiles(inputPath)
        for(file <- files) {
            for (line <- Source.fromFile(file).getLines) {
                val tokens = line.split(',')

                scala.util.control.Exception.ignoring(classOf[NumberFormatException]) {
                    val value = tokens(0)
                    val frequency = tokens(1).toDouble

                    if(value != "") {
                        values.append(value)
                        frequencies.append(frequency)
                    }
                }

                frequencies.append()
            }
        }

        Label(name, values.toList, frequencies.toList)
    }

    def parseDouble(str : String) : Double = {
        try {
            return Some(str.toDouble).get
        } catch {
            case _ : Throwable => return 0.0
        }
        0.0
    }


    def extractName(path : String) : String = {
        val nameStart = path.lastIndexOf("/") + 1
        val name = path.substring(nameStart)
        name
    }

    def getCsvFiles(dir: String) : List[File] = {
        val d = new File(dir)
        if (d.exists && d.isDirectory) {
            d.listFiles.filter(file => file.isFile && file.getName.endsWith("csv")).toList
        } else {
            List[File]()
        }
    }

    def main(args: Array[String]): Unit = {
        print("Done.")
    }
}
