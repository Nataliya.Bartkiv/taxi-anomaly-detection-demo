package taxidemo.generator

import taxidemo.entities.TaxiTrip

object TaxiTripGenerator {

    def next() : TaxiTrip = {
        val dropoff_latitude : Float = 0F
        val dropoff_longitude : Float = 0F

        val drop_zone_data = TaxiTripMetadata.labels("dropoff_taxizone_id")
        val dropoff_taxizone_id : Int = Randomizer.nextLabel(drop_zone_data.values, drop_zone_data.frequencies).getOrElse("0").toInt

        val ehail_fee : Float = 0F
        val extra : Float = 0F
        val fare_amount : Float = 0F
        val improvement_surcharge : Float = 0F
        val mta_tax : Float = 0F

        val passenger_data = TaxiTripMetadata.features("passenger_count")
        val passenger_count : Int = Randomizer.nextFeature(passenger_data).toInt

        val payment_data = TaxiTripMetadata.labels("payment_type")
        val payment_type : String = Randomizer.nextLabel(payment_data.values, payment_data.frequencies).getOrElse("")

        val pickup_latitude : Float = 0L
        val pickup_longitude : Float = 0L

        val pickup_zone_data = TaxiTripMetadata.labels("pickup_taxizone_id")
        val pickup_taxizone_id : Int = Randomizer.nextLabel(pickup_zone_data.values, pickup_zone_data.frequencies).getOrElse("0").toInt

        val rate_data = TaxiTripMetadata.labels("rate_code_id")
        val rate_code_id : Int = Randomizer.nextLabel(rate_data.values, rate_data.frequencies).getOrElse("0").toInt

        val flag_data = TaxiTripMetadata.labels("store_and_fwd_flag")
        val store_and_fwd_flag : String = Randomizer.nextLabel(flag_data.values, flag_data.frequencies).getOrElse("")

        val tip_data = TaxiTripMetadata.features("tip_amount")
        val tip_amount : Float = Randomizer.nextFeature(tip_data).toFloat

        val tolls_amount : Float = 0F

        val total_amount_data = TaxiTripMetadata.features("total_amount")
        val total_amount : Float = Randomizer.nextFeature(total_amount_data).toFloat

        val distance_data = TaxiTripMetadata.features("trip_distance")
        val trip_distance : Float = Randomizer.nextFeature(distance_data).toFloat

        val type_data = TaxiTripMetadata.labels("trip_type")
        val trip_type : String = Randomizer.nextLabel(flag_data.values, flag_data.frequencies).getOrElse("")

        val vendor_data = TaxiTripMetadata.labels("vendor_id")
        val vendor_id : String = Randomizer.nextLabel(vendor_data.values, vendor_data.frequencies).getOrElse("")

        val trip_id : Long = 0L


        val trip_duration_data = TaxiTripMetadata.features("trip_duration")
        val trip_duration_seconds = Randomizer.nextFeature(trip_duration_data).toLong
        val dropoff_datetime : Long = System.currentTimeMillis
        val pickup_datetime : Long = dropoff_datetime - trip_duration_seconds

        TaxiTrip(dropoff_datetime, dropoff_latitude, dropoff_longitude, dropoff_taxizone_id,
            ehail_fee, extra, fare_amount, improvement_surcharge,
            mta_tax, passenger_count, payment_type,
            pickup_datetime, pickup_latitude, pickup_longitude, pickup_taxizone_id,
            rate_code_id, store_and_fwd_flag, tip_amount, tolls_amount, total_amount,
            trip_distance, trip_type, vendor_id, trip_id)
    }

    def main(args: Array[String]): Unit = {
        val vendorsData = TaxiTripMetadata.labels("vendor_id")

        for(_ <- 0 until 100) {
            println(next())
        }
    }
}
