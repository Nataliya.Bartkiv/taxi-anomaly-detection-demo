package taxidemo.generator

import taxidemo.entities.Feature

import scala.collection.mutable
import scala.util.Random

object Randomizer {
    protected def getCumulativeFrequencies(frequencies: List[Double]): List[Double] = {
        val length = frequencies.length
        val cumulative: Array[Double] = Array.fill(length) {
            0
        }
        cumulative(0) = frequencies.head
        for (index <- 1 until length) {
            cumulative(index) = cumulative(index - 1) + frequencies(index)
        }
        cumulative.toList
    }

    protected def getClosestIndex(list: List[Double], value: Double): Int = {
        val foundValue = list.minBy(v => math.abs(v - value))
        list.indexOf(foundValue)
    }

    def nextLabel(values: List[String], frequencies: List[Double]): Option[String] = {
        if (values.nonEmpty) {
            val random = Random.nextDouble()
            val cumulative = getCumulativeFrequencies(frequencies)
            val randomIndex = getClosestIndex(cumulative, random)
            return Option(values(randomIndex))
        }
        None
    }

    def nextFeature(feature : Feature) : Double = {
        var deviation = nextDouble(0, feature.variance)

        val isDeviationNegative = Random.nextBoolean()
        if(isDeviationNegative && deviation < feature.average) {
            deviation *= -1
        }

        val value = feature.average + deviation
        value
    }

    def nextDouble(from: Double, to: Double): Double = {
        if (from == to) {
            from
        } else {
            val mid = from + (to/2 - from/2)
            if (Random.nextBoolean) nextDouble(from, mid) else nextDouble(mid, to)
        }
    }


    def main(args: Array[String]): Unit = {
        val test_number = 1000
        val values = List("banana", "apple", "pineapple")
        val frequencies = List(0.4, 0.5, 0.1)

        val dict = mutable.HashMap[String, Int]()

        for (value <- values) {
            dict.update(value, 0)
        }

        for (_ <- 0 until test_number) {
            val generated = nextLabel(values, frequencies).get
            val newValue = dict(generated) + 1

            dict.update(generated, newValue)
        }

        print(dict)
    }
}
