import com.databricks.spark.avro._
import org.apache.spark.sql.{SaveMode, SparkSession}


object FormatSwitcher {
    val DEFAULT_PARQUET_PATH = "data/parquet"
    val DEFAULT_CSV_PATH = "data/csv"
    val DEFAULT_AVRO_PATH = "data/avro"

    def parquetToCsv(spark : SparkSession, inputPath : String, outputPath : String, header : Boolean = true) : Unit = {
        val inputDF = spark.read.parquet(inputPath)
        inputDF.write.option("header", header).mode(SaveMode.Overwrite).csv(outputPath)
    }

    def parquetToAvro(spark : SparkSession, inputPath : String, outputPath : String) : Unit = {
        val inputDF = spark.read.parquet(inputPath)
        val fixedDF = inputDF.na.fill(0,
            Seq("dropoff_datetime", "dropoff_latitude", "dropoff_longitude", "dropoff_taxizone_id",
                "ehail_fee", "extra", "fare_amount", "improvement_surcharge", "mta_tax", "passenger_count",
                "pickup_datetime", "pickup_latitude", "pickup_longitude", "pickup_taxizone_id",
                "rate_code_id", "tip_amount", "tolls_amount", "total_amount", "trip_distance", "trip_id"))
                .na.fill("", Seq("payment_type", "store_and_fwd_flag", "trip_type", "vendor_id"))

        fixedDF.write.mode(SaveMode.Overwrite).avro(outputPath)
    }

    def main(args: Array[String]): Unit = {
        val appName = "Format Switcher"
        val master = "local[*]"
        var inputPath = ""
        var outputPath = ""

        if (args.length != 0) {
            inputPath = args(0)
            outputPath = args(1)
        } else {
            inputPath = DEFAULT_PARQUET_PATH
            outputPath = DEFAULT_AVRO_PATH
//            outputPath = DEFAULT_CSV_PATH
        }

        val spark = SparkSession.builder().appName(appName).master(master).getOrCreate()

        parquetToAvro(spark, inputPath, outputPath)
//        parquetToCsv(spark, inputPath, outputPath, false)
        print(s"Successfully saved data from '$inputPath' to '$outputPath'.")
    }
}

