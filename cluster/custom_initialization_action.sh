# Before running script, do these actions:
# sudo mkdir /shared
# cd /shared
# sudo git clone https://gitlab.com/Nataliya.Bartkiv/taxi-anomaly-detection-demo.git 
# (Enter credentials)
# sudo chmod -R ugo+rw /shared/
# sudo chmod ugo+x /shared/taxi-anomaly-detection-demo/cluster/custom_initialization_action.sh

# Install java
# sudo mkdir /usr/share/java8
# cd /usr/share/java8
# sudo wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-linux-x64.tar.gz
# sudo tar zxf jdk-8u171-linux-x64.tar.gz
# export JAVA_HOME=/usr/share/java8/jdk1.8.0_171
# sudo rm jdk-8u171-linux-x64.tar.gz

readonly ROLE="$(/usr/share/google/get_metadata_value attributes/dataproc-role)"
export PROJECT_HOME=/shared/taxi-anomaly-detection-demo/

echo '******************* Changing python permissions *******************' 
sudo chmod -R ugo+rw /opt/conda/lib/python3.6/
sudo chmod -R ugo+rw /opt/conda/

echo '******************* Installing python dependencies *******************'
pip install --upgrade pip

pip install cython
pip install pandas
pip install matplotlib
pip install scipy
pip install seaborn
pip install sklearn
pip install tensorflowonspark
pip install findspark
pip install keras

echo '******************* Installing pydoop *******************'

sudo apt-get --assume-yes install python3-dev
git clone -b master https://github.com/crs4/pydoop.git
cd pydoop
python setup.py build
python setup.py install --skip-build

echo '******************* Creating shared folder *******************'
# sudo mkdir  /home/shared
# cd /home/shared
# sudo chmod -R ugo+xrw /home/shared

# sudo mkdir /shared
sudo chmod -R ugo+rwx /shared

mkdir /shared/creditcard
mkdir /shared/creditcard/logs
mkdir /shared/creditcard/models

echo '******************* Cloning TensorflowOnSpark repo *******************'
git clone https://github.com/yahoo/TensorFlowOnSpark.git
pushd TensorFlowOnSpark
zip -r tfspark.zip tensorflowonspark
popd

echo '******************* Installing Confluent *******************'

sudo apt-get --assume-yes install software-properties-common
sudo apt-get --assume-yes install apt-transport-https

wget -qO - https://packages.confluent.io/deb/4.1/archive.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.confluent.io/deb/4.1 stable main"
sudo apt-get update && sudo apt-get --assume-yes install confluent-platform-oss-2.11

echo '******************* Configure python virtual environment *******************'


cd $PROJECT_HOME/python/taxi-anomaly-detection
virtualenv venv --distribute --python=/opt/conda/bin/python3.6
source venv/bin/activate
pip install -r requirements.txt
deactivate


if [[ "${ROLE}" == 'Master' ]]; then
	# specific actions here

	echo '******************* Create hdfs folders *******************'

	hdfs dfs -mkdir /schemas
	hdfs dfs -mkdir /shared
	hdfs dfs -chmod -R ugo+rw /shared

	echo '******************* Create Hive tables *******************'

	export SCHEMA_FILE=$PROJECT_HOME/cluster/cube-schema.avsc
	
	hdfs dfs -put $SCHEMA_FILE /schemas

	hive -e "CREATE DATABASE nyt"
	hive -e "create table nyt.trips_cube 
                stored as avro 
                location 'gs://tsof-anomaly-detection-bucket/nyt-cube'
                tblproperties('avro.schema.url' = '/schemas/cube-schema.avsc')"

	hive -e "create external table nyt.trips (
                dropoff_datetime        timestamp,
                dropoff_latitude        float,
                dropoff_longitude       float,
                dropoff_taxizone_id     int,
                ehail_fee               float,
                extra                   float,
                fare_amount             float,
                improvement_surcharge   float,
                mta_tax                 float,
                passenger_count         int,
                payment_type            string,
                pickup_datetime         timestamp,
                pickup_latitude         float,
                pickup_longitude        float,
                pickup_taxizone_id      int,
                rate_code_id            int,
                store_and_fwd_flag      string,
                tip_amount              float,
                tolls_amount            float,
                total_amount            float,
                trip_distance           float,
                trip_type               string,
                vendor_id               string,
                trip_id                 bigint,
                pickup_year             int,
                pickup_month            int)
          STORED AS PARQUET
          LOCATION 'gs://tsof-anomaly-detection-bucket/nyt';"

	echo '******************* Install maven *******************'

	apt-cache search maven
	sudo apt-get --assume-yes install maven

	echo '******************* Start confluent *******************'
	confluent start

fi

if [[ "${ROLE}" == 'Worker' ]]; then
	# specific actions here

	echo '******************* Configure Kafka server properties *******************'
	sudo cp /etc/kafka/server.properties /etc/kafka/server.properties.template

	export KAFKA_BROKER_FILE=/etc/kafka/server.properties
	export WORKER_ID=$(hostname | sed 's/.*-w-\([0-9]\)*.*/\1/g')
	export BROKER_ID=$((100+$WORKER_ID))
	export MASTER=$(hdfs getconf -confKey yarn.resourcemanager.hostname)
	export ZOOKEEPER_URL="${MASTER}:2181"

	sudo sed -i 's,^\(broker\.id=\).*,\1'${BROKER_ID}',' $KAFKA_BROKER_FILE
	sudo sed -i 's,^\(zookeeper\.connect=\).*,\1'${ZOOKEEPER_URL}',' $KAFKA_BROKER_FILE
	sudo sed -i '/#listeners=/ i listeners=PLAINTEXT://'$(hostname)':9092' $KAFKA_BROKER_FILE
fi

echo '******************* Finished *******************'
